import mongoose   from 'mongoose';
import User       from'../../mongoose/user.model';
import Contact    from'../../mongoose/contact.model';
import Note       from'../../mongoose/note.model';
import Task       from'../../mongoose/task.model';
import test_users from './users.json.js';
import test_data  from './test_data.json.js';

mongoose.Promise = global.Promise;

const log = console.log;
const exit_with_error = function (msg: string, err: string) {
	console.error(msg, err);
	log('Exiting...');
	process.exit();
};

async function seed () {
	// Connect to DB
	try {
		await mongoose.connect('mongodb://localhost:27017/strix')
	} catch (err) {
		exit_with_error('Connection error:', err);
	}
	log('MongoDB is connected...');

	// Find user
	let user: any;
	try {
		user = await User.findOne({username: test_users[0].username});
	} catch (err) {
		exit_with_error('Could find user:', err);
	}
	log('User found...');

	// Clear items of user
	try {
		await Contact.deleteMany({owner: user._id});
		await Note.deleteMany({owner: user._id});
		await Task.deleteMany({owner: user._id});
	} catch (err) {
		exit_with_error('Could not clear items:', err);
	}
	log('Items cleared...');

	// Insert test items
	const item_promises = [];

	for (let contact_data of test_data.contacts) {
		const contact = new Contact({
			...contact_data,
			owner: user.id
		});
		item_promises.push(contact.save());
	}

	for (let note_data of test_data.notes) {
		const note = new Note({
			...note_data,
			owner: user.id
		});
		item_promises.push(note.save());
	}

	for (let task_data of test_data.tasks) {
		const task = new Task({
			...task_data,
			owner: user.id
		});
		item_promises.push(task.save());
	}

	try {
		await Promise.all(item_promises);
	} catch (err) {
		exit_with_error('Could not seed all items:', err);
	}

	log('All items successfully seeded...');
	process.exit();
};

seed();
