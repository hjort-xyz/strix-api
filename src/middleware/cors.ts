import config from '../config';
import cors from 'cors';

export default cors({
	origin: config.webapp,
	methods: 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
	credentials: true,
	allowedHeaders: 'Origin, X-Requested-With, Content-Type, Accept, Authorization, authorization'
});