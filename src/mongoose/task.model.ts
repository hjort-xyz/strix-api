import mongoose from "mongoose";
import Syncable from "./syncable.model";

const taskSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true,
		minlength: 1,
		maxlength: 128
	},
	description: {
		type: String,
		required: false,
		maxlength: 128
	},
	completedAt: {
		type: Date
	},
	cooldownTime: {
		type: Number,
		default: Infinity 
	},
	hash: {
		type: String,
		required: false,
		default: null,
		maxlength: 32,
		set: function(hash: string) {
			const self: any = this
			self._old_hash = self.hash
			return hash;
		}
	},
	...Syncable.properties
}, {
	timestamps: true,
	...Syncable.options
});

taskSchema.pre('save', Syncable.hasher);

export default mongoose.model("Task", taskSchema);
