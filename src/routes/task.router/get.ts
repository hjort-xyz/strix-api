import {
	Request,
	Response
} from 'express';
import {
	Task_handlers
} from '../../models/task';
import log from '../../util/log';

async function get_all (req: Request, res: Response): Promise<void> {
	const { id: owner } = req.user;
	
	// Find Tasks
	try {
		const tasks = await Task_handlers.get_all(owner);
		
		// Success response
		res.json(tasks);
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send({ error: 'Could not find any tasks' });
		return;
	}
};

async function get_one (req: Request, res: Response): Promise<void>  {
	const { id: owner } = req.user;
	const { id } = req.params;

	// Find Task
	try {
		const task = await Task_handlers.get(id as string, owner as string);
		
		// Success response
		res.json(task);
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send({ error: 'Could not find task' });
		return;
	}
};

export {
	get_all,
	get_one
};