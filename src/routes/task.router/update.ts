import {
	Request,
	Response
} from 'express';
import {
	Task,
	Task_handlers
} from '../../models/task';
import log from '../../util/log';

async function update_one (req: Request, res: Response): Promise<void>  {
	const { id: owner } = req.user;
	const { id } = req.params;
	const {
		title,
		description,
		completedAt,
		cooldownTime
	} = req.body;
	
	// Find task
	try {
		const task = await Task_handlers.update({
			id,
			owner,
			title,
			description,
			completedAt,
			cooldownTime
		} as Task);
		
		// Success response
		res.json(task);
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send('Could not update task');
		return;
	}
}

export {
	update_one
};