import {
	Request,
	Response
} from 'express';
import {
	Note,
	Note_handlers
} from '../../models/note';
import log from '../../util/log';

async function create_one (req: Request, res: Response): Promise<void>  {
	const { id: owner } = req.user;
	const {
		title,
		body
	} = req.body;

	// Create note
	try {
		const note: Note = await Note_handlers.create({
			id: undefined,
			owner,
			has_access: [],
			deleted: false,
			title,
			body,
			hash: undefined
		});
		if (!note) throw new Error('Could not create note');
		
		// Success response
		res.json(note);
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send({ error: 'Could not create note' });
		return;
	}
};

export {
	create_one
};
