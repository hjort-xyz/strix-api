import {
	Request,
	Response
} from 'express';
import {
	Note,
	Note_handlers
} from '../../models/note';
import log from '../../util/log';

async function update_one (req: Request, res: Response): Promise<void>  {
	const { id: owner } = req.user;
	const { id } = req.params;
	const {
		title,
		body
	} = req.body;
	
	// Find note
	try {
		const note = await Note_handlers.update({
			id,
			owner,
			title,
			body
		} as Note);
		
		// Success response
		res.json(note);
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send('Could not update note');
		return;
	}
}

export {
	update_one
};