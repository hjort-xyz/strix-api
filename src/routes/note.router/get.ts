import {
	Request,
	Response
} from 'express';
import {
	Note_handlers
} from '../../models/note';
import log from '../../util/log';

async function get_all (req: Request, res: Response): Promise<void> {
	const { id: owner } = req.user;
	
	// Find Notes
	try {
		const notes = await Note_handlers.get_all(owner);
		
		// Success response
		res.json(notes);
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send({ error: 'Could not find any notes' });
		return;
	}
};

async function get_one (req: Request, res: Response): Promise<void>  {
	const { id: owner } = req.user;
	const { id } = req.params;

	// Find Note
	try {
		const note = await Note_handlers.get(id as string, owner as string);
		
		// Success response
		res.json(note);
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send({ error: 'Could not find note' });
		return;
	}
};

export {
	get_all,
	get_one
};