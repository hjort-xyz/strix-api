import {
	Request,
	Response,
	Router
} from 'express';
import {
	User,
	get_user,
	verify_user_password,
	update_user
} from '../models/user';
import log from '../util/log';
const user_router = Router();

// GET /user
user_router.get('/', async function (req: Request, res: Response) {
	const { id } = req.user;
	
	// Find user
	try {
		const user: User = await get_user(id);
		if (!user) throw new Error('No user found');

		// Success response
		res.json({ user });
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send({ error: 'Could not find user' });
		return;
	}
});

// PATCH /user
user_router.patch('/', async function (req: Request, res: Response) {
	const { id } = req.user;
	const { old_password, new_password } = req.body;
	
	// Find user
	try {
		const user = await get_user(id);
		if (!user) throw new Error('No user found');
		
		// Check that old password matches
		const match = await verify_user_password(id, old_password);
		if (!match) throw new Error('Authentication failed');

		// Udate user with new password
		user.password = new_password;
		await update_user(user);
		
		// Success response
		res.json(user);
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send(error);
		return;
	}
});

export default user_router;
