import {
	Request,
	Response
} from 'express';
import {
	Contact,
	Contact_handlers
} from '../../models/contact';
import log from '../../util/log';

async function update_one (req: Request, res: Response): Promise<void>  {
	const { id: owner } = req.user;
	const { id } = req.params;
	const {
		firstname,
		middlename,
		lastname,
		phone_numbers,
		emails,
		birthdate,
		other_services
	} = req.body;

	// Update contact
	try {
		const contact = await Contact_handlers.update({
			id,
			owner,
			firstname,
			middlename,
			lastname,
			phone_numbers,
			emails,
			birthdate,
			other_services
		} as Contact);
		
		// Success response
		res.json(contact);
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send('Could not update contact');
		return;
	}
}

export {
	update_one
};