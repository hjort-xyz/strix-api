import { Router }           from 'express';
import { create_one }       from './create';
import { get_all, get_one } from './get';
import { update_one }       from './update';
import { delete_one }       from './delete';

const contact_router: Router = Router();

// GET /contact
contact_router.get('/', get_all);
contact_router.get('/:id', get_one);

// POST /contact
contact_router.post('/', create_one);

// PATCH /contact
contact_router.patch('/:id', update_one);

// DELETE /contact
contact_router.delete('/:id', delete_one);

export default contact_router;
