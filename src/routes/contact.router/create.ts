import {
	Request,
	Response
} from 'express';
import {
	Contact,
	Contact_handlers
} from '../../models/contact';
import log from '../../util/log';

async function create_one (req: Request, res: Response): Promise<void>  {
	const { id: owner } = req.user;
	const {
		firstname,
		middlename,
		lastname,
		phone_numbers,
		emails,
		birthdate,
		other_services
	} = req.body;

	// Create Contact
	try {
		const contact: Contact = await Contact_handlers.create({
			id: undefined,
			owner,
			has_access: [],
			deleted: false,
			firstname,
			middlename,
			lastname,
			phone_numbers,
			emails,
			birthdate,
			other_services,
			hash: undefined
		});

		// Success response
		res.json(contact);
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send({ error: 'Could not create contact' });
		return;
	}
};

export {
	create_one
};
