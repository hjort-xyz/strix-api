import {
	Request,
	Response,
	Router
} from 'express';
import {
	Task_handlers
} from '../models/task';
import log from '../util/log';
const status_router = Router();

// GET /status
status_router.get('/', async function (req: Request, res: Response) {
	const { id: owner } = req.user;
	
	// Count collections
	try {
		const task_last_updated: number = await Task_handlers.last_updated(owner);

		// Success response
		res.json({
			last_updated: {
				task: task_last_updated
			}
		});
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send({ error: 'Could not return status' });
		return;
	}
});

export default status_router;
