import {
    Syncable_handlers,
    syncable_handlers
} from '../syncable';
import mongoose from 'mongoose';


const handlers: Syncable_handlers = {
    ...syncable_handlers,
    persister: mongoose.model('Task'),

    update: async function (this: Syncable_handlers, updated_task: any) {
        const task = <any>await this.persister.findOne({
            _id: updated_task.id,
            owner: updated_task.owner
        });
        if (!task) throw new Error('No task found');

        task.title        = updated_task.title        || task.title;
        task.description  = updated_task.description  || task.description;
        task.completedAt  = updated_task.completedAt  || task.completedAt;
        task.cooldownTime = updated_task.cooldownTime || task.cooldownTime;
        
        const persisted_task = await task.save();
        return this.strip(persisted_task);
    },

    strip: function <Task>(task: any) {
        const {
            _id,
            owner,
            has_access,
            title,
            description,
            completedAt,
            cooldownTime,
            hash
        } = task;

        return {
            id: _id,
            owner,
            has_access,
            title,
            description,
            completedAt,
            cooldownTime,
            hash
        } as unknown as Task;
    }
};

export default handlers;