import {
    Syncable_handlers,
    syncable_handlers
} from '../syncable';
import mongoose from 'mongoose';

const handlers: Syncable_handlers = {
    ...syncable_handlers,
    persister: mongoose.model('Note'),

    update: async function (this: Syncable_handlers, updated_note: any) {
        const note = <any>await this.persister.findOne({
            _id: updated_note.id,
            owner: updated_note.owner
        });
        if (!note) throw new Error('No note found');

        note.title = updated_note.title || note.title;
        note.body  = updated_note.body  || note.body;

        const persisted_note = await note.save();
        return this.strip(persisted_note);
    },

    strip: function <Note>(note: any) {
        const {
            _id,
            owner,
            has_access,
            title,
            body,
            hash
        } = note;

        return {
            id: _id,
            owner,
            has_access,
            title,
            body,
            hash
        } as unknown as Note;
    }
};

export default handlers;