import {
    Syncable_handlers,
    syncable_handlers
} from '../syncable';
import mongoose from 'mongoose';

const handlers: Syncable_handlers = {
    ...syncable_handlers,
    persister: mongoose.model('Contact'),
    
    update: async function (this: Syncable_handlers, updated_contact: any) {
        const contact = <any>await this.persister.findOne({
            _id: updated_contact.id,
            owner: updated_contact.owner
        });
        if (!contact) throw new Error('No contact found');

        contact.firstname      = updated_contact.firstname      || contact.firstname;
        contact.middlename     = updated_contact.middlename     || contact.middlename;
        contact.lastname       = updated_contact.lastname       || contact.lastname;
        contact.phone_numbers  = updated_contact.phone_numbers  || contact.phone_numbers;
        contact.emails         = updated_contact.emails         || contact.emails;
        contact.birthdate      = updated_contact.birthdate      || contact.birthdate;
        contact.other_services = updated_contact.other_services || contact.other_services;
        
        const persisted_contact = await contact.save();
        return this.strip(persisted_contact);
    },
    
    strip: function <Contact>(contact: any) {
        const {
            _id,
            owner,
            has_access,
            firstname,
            middlename,
            lastname,
            phone_numbers,
            emails,
            birthdate,
            other_services,
            hash
        } = contact;

        return {
            id: _id,
            owner,
            has_access,
            firstname,
            middlename,
            lastname,
            phone_numbers,
            emails,
            birthdate,
            other_services,
            hash
        } as unknown as Contact;
    }
};

export default handlers;