type Syncable = {
    id?: string
	owner: string
	has_access?: Array<string>
	deleted?: Boolean
	hash?: string
};

export default Syncable;