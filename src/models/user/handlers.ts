import User from './type';
import mongoose from 'mongoose';

const User_persister = mongoose.model('User');

const get_user = async (id: string, key: string = 'id'): Promise<User> => {
    
    let user: User
    if (key === 'username') {        
        user = <any>await User_persister.findOne({ username: id });
    } else if (key === 'id') {
        user = <any>await User_persister.findById(id);
    } else {
        throw new Error('Incorrect usage of get_user: second argument must be "id" or "username"');
        
    }
    if (!user) throw new Error('No user found');
    
    return strip_user(user);
}

const update_user = async (updated_user: User): Promise<User> => {
	const user = <any>await User_persister.findById(updated_user.id);
    if (!user) throw new Error('No user found');

    user.password = updated_user.password || user.password;
    
    const persisted_user = await user.save();
    return strip_user(persisted_user);
}

const verify_user_password = async (id: string, given_password: string): Promise<Boolean> => {
	const user = <any>await User_persister.findById(id);
    if (!user) throw new Error('No user found');

    return user.compare_password(given_password);
}

const strip_user = (user: any): User => {
    const {
        _id,
        username
    } = user;

    return {
        id: _id,
        username
    } as User;
}

export {
    get_user,
    update_user,
    verify_user_password
};